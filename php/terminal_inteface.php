function getTerminalSize($which = null) {
    $terminalSize = array(
        'columns'   => -1,
        'rows'      => -1
    );

    $osName = mb_strtolower(PHP_OS);
    if (false !== mb_strpos($osName, 'win')) {
        $terminalSize = getWindowsTerminalSize();
    }
    if (false !== mb_strpos($osName, 'linux')) {
        $terminalSize = getLinuxTerminalSize();
    }
    $columns    = $terminalSize['columns'];
    $rows       = $terminalSize['rows'];

    switch ($which) {
        case 'columns':
        case 'cols':
        case 'x':
        case 'width':
            return $columns;
            break;
        case 'rows':
        case 'y':
        case 'height':
            return $rows;
            break;
        default:
        case null:
            return array(
                'columns'   => $columns,
                'rows'      => $rows,
            );
            break;
    }
}

function getLinuxTerminalSize() {
    return array(
        'columns'       => exec('tput cols'),
        'rows'          => exec('tput lines')
    );
}

function getWindowsTerminalSize() {
    $columns    = -1;
    $rows       = -1;
    $output = null;
    exec('mode con', $output);
    foreach ($output as $lineNumber => $line) {
        $line = mb_strtolower(trim($line));
        if (0 >= mb_strlen($line)) {
            continue;
        }

        if (0 < $columns && 0 < $rows) {
            break;
        }

        if (false !== mb_strpos($line, 'columns')) {
            list($index, $value) = explode(':', $line);
            $columns = intval($value);
            continue;
        }

        if (false !== mb_strpos($line, 'lines')) {
            list($index, $value) = explode(':', $line);
            $rows = intval($value);
            continue;
        }
    }

    return array(
        'columns'    => $columns,
        'rows'       => $rows
    );
}
