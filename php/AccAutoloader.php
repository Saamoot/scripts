<?php

class AccAutoLoader
{
    /**
     * List of directories in which classes will be searched for.
     * One record is one directory.
     *
     * @var        array
     */
    private static $__sourceDirectories = [];
    /**
     * List of strings which will be prepended to className when searching in directories.
     * One record is one prefix.
     *
     * Example:
     *    Lets assume we have one record 'class.' in array and we are searching for class named 'customClass'.
     *  FileName for which search will be executed will look like this 'class.customClass.php'.
     *  '.php' is appended automatically.
     *
     * @var array
     */
    private static $__filePrefixes = [];
    /**
     * List of strings which will be appended to className when searching in directories.
     * One record is one postfix.
     *
     * Example:
     *    Lets assume we have one record '.lib' in array and we are searching for class named 'customClass'.
     *  FileName for which search will be executed will look like this 'customClass.lib.php'.
     *  '.php' is appended automatically.
     *
     * @var array
     */
    private static $__filePostfixes = [];
    /**
     * List of strings which will be used as file extensions when loader is searching for file.
     * Records should be stored without leading dot.
     *
     * @var array
     */
    private static $__fileExtensions = [
        'php',
        'inc'
    ];
    /**
     * Variable for storing path to project/application directory. This is for
     * providing absolute paths to files.
     *
     * @var null|string
     */
    private static $__rootDir = null;
    /**
     * Variable for storing callback method when no file is found for desired class. This callback
     * will be called after search and will get className as input argument.
     *
     * Example:
     *   - value can be anonymous function: function($className, $classFilename) {....}
     */
    private static $__callBack = null;

    /**
     * Cache variable for storing map of class names and files where they were found.
     *
     * @var array
     */
    private static $__classMap = [];

    /**
     * Internal limit how many levels of sub-directories will be searched.
     *
     * @var int
     */
    private const    SUB_DIRECTORY_DEPTH_LIMIT = 5;

    /**
     * Value returned when method executed successfully.
     *
     * @var int
     */
    public const METHOD_RESPONSE_SUCCESS = 0;
    /**
     * Value returned when provided directory path doesn't exist.
     *
     * @var int
     */
    public const METHOD_RESPONSE_INVALID_DIRECTORY = 1;
    /**
     * Value returned when provided directory path was already added.
     *
     * @var int
     */
    public const METHOD_RESPONSE_DIRECTORY_ALREADY_EXIST = 2;

    public const METHOD_RESPONSE_CANNOT_READ_DIR = 4;

    /**
     * setRootDir method
     *
     * Sets new root directory path. If there were added "sourceDirectories" false will be
     * returned because added paths may be relative. If specified path isn't directory false
     * will be returned. On success true is returned.
     *
     * By setting root dir, all search for class files will be using absolute paths.
     *
     * If path doesn't end with DIRECTORY_SEPARATOR it will be added.
     *
     * @uses AccAutoLoader::$__rootDir
     *
     * @param    string $newRootDir
     *
     * @return    bool
     */
    public static function setRootDir(string $newRootDir) : bool
    {
        if (count(self::$__sourceDirectories) > 0) {
            return false;
        }

        if (is_dir($newRootDir) === false) {
            return false;
        }

        if ($newRootDir[mb_strlen($newRootDir) - 1] !== DIRECTORY_SEPARATOR) {
            $newRootDir .= DIRECTORY_SEPARATOR;
        }

        self::$__rootDir = $newRootDir;

        return true;
    }

    /**
     * addDirectory method
     *
     * Add new source directory where classes can be located.
     *
     * @uses AccAutoLoader::$__rootDir
     * @uses AccAutoLoader::$__sourceDirectories
     * @uses AccAutoLoader::addDirectory()
     * @uses AccAutoLoader::METHOD_RESPONSE_SUCCESS
     * @uses AccAutoLoader::METHOD_RESPONSE_INVALID_DIRECTORY
     * @uses AccAutoLoader::METHOD_RESPONSE_DIRECTORY_ALREADY_EXIST
     *
     * @param   string $directoryName                  Path to directory
     * @param   int    $searchInSubDirectoriesMaxDepth Number how deep in specified directory will be searched if it contains sub-directories.
     *
     * @return  int                                          Method response which represents status of adding new source directory.
     */
    public static function addDirectory(string $directoryName, int $searchInSubDirectoriesMaxDepth = 0) : int
    {
        if ($directoryName[mb_strlen($directoryName) - 1] !== DIRECTORY_SEPARATOR) {
            $directoryName .= DIRECTORY_SEPARATOR;
        }

        if (
            is_null(self::$__rootDir) === false &&
            mb_strpos($directoryName, self::$__rootDir) === false &&
            self::__isAbsolutePath($directoryName) === false
        ) {
            $directoryName = self::$__rootDir . $directoryName;
        }

        if (intval($searchInSubDirectoriesMaxDepth) > 0) {
            $searchInSubDirectoriesMaxDepth = $searchInSubDirectoriesMaxDepth % self::SUB_DIRECTORY_DEPTH_LIMIT;
            self::__addSubDirectories($directoryName, $searchInSubDirectoriesMaxDepth);
        }

        if (in_array($directoryName, self::$__sourceDirectories)) {
            return self::METHOD_RESPONSE_DIRECTORY_ALREADY_EXIST;
        }

        if (is_dir(mb_substr($directoryName, 0, -1)) === false) {
            return self::METHOD_RESPONSE_INVALID_DIRECTORY;
        }

        $addedItems = array_push(self::$__sourceDirectories, $directoryName);
        if ($addedItems > 0) {
            return self::METHOD_RESPONSE_SUCCESS;
        } else {
            return self::METHOD_RESPONSE_DIRECTORY_ALREADY_EXIST;
        }
    }

    private static function __isAbsolutePath(string $directorName) : bool
    {
        if (mb_strpos(mb_strtolower(PHP_OS), 'win') >= 0) {
            return (preg_match('/\A[A-Za-z]{1}:.*/', $directorName) === 1);
        }

        if (mb_strpos(mb_strtolower(PHP_OS), 'linux') >= 0) {
            return ($directorName[0] === '/');
        }

        return false;
    }

    /**
     * addPostfix method
     *
     * Adds postfix string record to array of postfixes.
     *
     * @uses AccAutoLoader::$__filePostfixes
     *
     * @param string $newPostfix
     */
    public static function addPostfix(string $newPostfix) : void
    {
        array_push(
            self::$__filePostfixes,
            $newPostfix,
            mb_strtolower($newPostfix),
            mb_strtoupper($newPostfix),
            ucfirst($newPostfix)
        );
    }

    /**
     * addPrefix method
     *
     * Adds prefix string record to array of prefixes.
     *
     * @uses AccAutoLoader::$__filePrefixes
     *
     * @param string $newPrefix
     */
    public static function addPrefix(string $newPrefix) : void
    {
        array_push(
            self::$__filePrefixes,
            $newPrefix,
            mb_strtolower($newPrefix),
            mb_strtoupper($newPrefix),
            ucfirst($newPrefix)
        );
    }

    /**
     * addFileExtension method
     *
     * Adds extension string record to array of extensions.
     *
     * @uses AccAutoLoader::$__fileExtensions
     *
     * @param string $newFileExtension
     */
    public static function addFileExtension(string $newFileExtension) : void
    {
        if ($newFileExtension[0] === '.') {
            $newFileExtension = substr($newFileExtension, 1);
        }

        array_push(self::$__fileExtensions, $newFileExtension);
    }

    /**
     * addCallback method
     *
     * If the new callback is callable or is array containing exactly
     * two items: object and string, argument will be added as new
     * callback.
     *
     * @uses AccAutoLoader::$__callBack
     *
     * @param    callable|array $newCallback
     */
    public static function addCallback($newCallback) : void
    {
        if (is_callable($newCallback)) {
            self::$__callBack = $newCallback;
        }

        if (
            is_array($newCallback) &&
            count($newCallback) === 2 &&
            is_object($newCallback[0]) &&
            is_string($newCallback[1])

        ) {
            self::$__callBack = $newCallback;
        }
    }

    /**
     * __autoLoaderPerNameSpace method
     *
     * Parse className with namespace and converts namespace part to path to directory and
     * adds it to source directories and calls method to load file via only className.
     *
     * @uses AccAutoLoader::addDirectory()
     *
     * @param  string $className
     *
     * @return string
     */
    private static function __autoLoaderPerNameSpace(string $className) : string
    {
        $classNameRaw = $className;
        $className    = str_replace('/', DIRECTORY_SEPARATOR, $className);
        $className    = str_replace('\\', DIRECTORY_SEPARATOR, $className);

        $fileName  = basename($className);
        $fileSpace = str_replace(DIRECTORY_SEPARATOR . $fileName, '', $className);

        // nothing special just hit it, keep it simple stupid
        $classFile = self::$__rootDir . $className . '.php';

        if (true === file_exists($classFile)) {
            require_once $classFile;
            self::__callCallback($classNameRaw, $classFile);

            return $classFile;
        }

        // well it is something special lets find it

        $pathsToTry  = [];
        $directories = array_merge(self::$__sourceDirectories, [self::$__rootDir]);
        foreach (self::$__fileExtensions as $extension) {
            $pathsToTry[] = self::$__rootDir . $className . '.' . $extension;
            foreach (self::$__filePostfixes as $postfix) {
                $pathsToTry[] = self::$__rootDir . $fileSpace . DIRECTORY_SEPARATOR . $fileName . $postfix . '.' . $extension;
                foreach (self::$__filePrefixes as $prefix) {
                    $pathsToTry[] = self::$__rootDir . $fileSpace . DIRECTORY_SEPARATOR . $prefix . $fileName . $postfix . '.' . $extension;
                    foreach ($directories as $directory) {
                        $pathsToTry[] = $directory . $className . '.' . $extension;
                        $pathsToTry[] = $directory . $fileSpace . DIRECTORY_SEPARATOR . $prefix . $fileName . $postfix . '.' . $extension;
                        $pathsToTry[] = $directory . $fileSpace . DIRECTORY_SEPARATOR . $fileName . $postfix . '.' . $extension;
                        $pathsToTry[] = $directory . $fileSpace . DIRECTORY_SEPARATOR . $prefix . $fileName . '.' . $extension;
                    }
                }
            }
        }

        $pathsToTry = array_unique($pathsToTry);

        $classFile = null;
        foreach ($pathsToTry as $path) {
            if (false === file_exists($path)) {
                continue;
            }

            $classFile = $path;
            break;
        }

        require_once $path;
        self::__callCallback($classNameRaw, $path);

        return $classFile;
    }

    /**
     * autoLoader method
     *
     * Determines if supplied className contains namespace delimiters (backslash) and
     * call method for namespace class name or simple class name.
     *
     * @uses AccAutoLoader::__autoLoaderPerNameSpace()
     *
     * @param    string $className
     *
     * @throws \Exception
     */
    public static function autoLoader(string $className)
    {
        if (mb_strpos($className, '\\') === false) {
            throw new Exception('Loading classes without namespace is removed');
            // convert "_" to "\" and run standard resovling ?
        }

        if (true === array_key_exists($className, self::$__classMap)) {
            require_once self::$__classMap[$className];

            return;
        }

        $loadedFile = self::__autoLoaderPerNameSpace($className);

        if (true === is_string($loadedFile)) {
            self::$__classMap[$className] = $loadedFile;
        }
    }

    /**
     * registerLoader method
     *
     * Registers custom auto loader to php environment. If auto loader
     * is successfully registered true is returned, otherwise false will
     * be returned.
     *
     */
    public static function registerLoader() : bool
    {
        return spl_autoload_register('AccAutoLoader::autoLoader');
    }

    /**
     * __addSubDirectories method
     *
     *
     * @param    string $directory
     * @param    int    $depth
     */
    private static function __addSubDirectories(string $directory, int $depth) : void
    {
        $directoryFiles = scandir($directory);
        if ($directoryFiles === false) {
            return;
        }

        // lower remaining depth
        $depth--;

        // removing '.' and '..'
        unset($directoryFiles[0], $directoryFiles[1]);

        foreach ($directoryFiles as $directoryFile) {
            $directoryName = $directory . $directoryFile;
            if (is_dir($directoryName)) {
                self::addDirectory($directoryName, $depth);
            }
        }
    }

    private static function __callCallback($className, $classFilename)
    {
        if (true === is_null(self::$__callBack)) {
            return;
        }

        call_user_func(self::$__callBack, $className, $classFilename);
    }
}
