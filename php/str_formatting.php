/**
 * Formats supplied text and value into two column output. Text in first column is aligned to the right.
 *
 *
 * Method subtracts from max length spaces and length of delimiter so resulting string will "fit" into
 * specified max length.
 *
 *
 * When calculating character space for value argument, max length is multiplied by ratio to get number of
 * characters. In case result of multiplication have decimal part, result is "ceiled" to remove decimal part.
 *
 *
 * Usage example:
 *  Code:
 *      echo formattedTextValueOutput('Current sapi is',    php_sapi_name(),    ':', 30, 2/10);
 *      echo formattedTextValueOutput('Current os is',      PHP_OS,             ':', 30, 2/10);
 *  Max length + valueText ratio description:
 *      - with maxLength 30
 *      - and ratio 2/10 which is equal to 1/5 and/or 0.2
 *      - value will have 30 * 0.2 characters (evaluated: 6) and text will have (30 - 6) characters
 *  Will produce:
 *            Current sapi is : cli
 *              Current os is : WINNT
 *
 * @param   string    $text                   - description or meaning of value
 * @param   string    $value                  - value corresponding to text
 * @param   string    $delimiter              - most often single character to split/differentiate between text and value
 * @param   int       $maxLength              - max length of output per line
 * @param   float     $valueTextCharRatio     - e.g. 2/10 means 2 of 10 characters will be assigned to value string space
 *
 * @return  string
 */
function formattedTextValueOutput($text, $value, $delimiter, $maxLength, $valueTextCharRatio) {
    $formattedStr       = $text . ' ' . $delimiter . ' ' . $value;
    $maxLength          = intval($maxLength);

    if (0 >= $maxLength) {
        return $formattedStr . "\t\t" . '//Invalid max length argument';
    }

    $maxLength          = $maxLength - 2 - mb_strlen($delimiter);                                            // substract spaces and delimiter from max size
    $valueStrMaxLength  = ceil($valueTextCharRatio * $maxLength);
    $textStrMaxLength   = $maxLength - $valueStrMaxLength;


    /*
    $formattedStr       = '';
    $formattedStr       .= str_pad($text, $textStrMaxLength, ' ', STR_PAD_LEFT);
    $formattedStr       .= ' ' . $delimiter . ' ';
    $formattedStr       .= str_pad($value, $valueStrMaxLength, ' ', STR_PAD_RIGHT);   // not necessary
    */
    $formatString = '% ' . $textStrMaxLength . 's %s %s';
    $formattedStr = sprintf($formatString, $text, $delimiter, $value);

    return $formattedStr;
}