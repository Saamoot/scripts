function isCommandLineInterface()
{
    if (false !== mb_strpos(mb_strtolower(php_sapi_name()), 'cli')) {
        return true;
    }

    return false;
}

function isCommonGatewayInterface()
{
    if (
        (
            isset($_SERVER['GATEWAY_INTERFACE']) &&
            false !== mb_strpos(mb_strtolower($_SERVER['GATEWAY_INTERFACE']), 'cgi')
        ) ||
        mb_strpos(mb_strtolower(php_sapi_name()), 'cgi')
    ) {
        return true;
    }

    return false;
}