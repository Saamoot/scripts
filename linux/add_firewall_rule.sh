#iptables 
#	-A {INPUT/OUTPUT/FORWARD} 
#	-p {tcp/udp} 
#	-m {same as -p {tcp/udp}} 
#	--dport {port number or range 1234:5678} 
#	-j {ACCEPT/DROP}

chain='null'		# INPUT, OUTPUT, FORWARD
protocol='null'		# tcp, udp
portNumber='0'		# 1,2,3.... or range 1:1000,27000:27100
action='null'		# ACCEPT, DROP

echo "Select chain:"
echo "	- INPUT"
echo " 	- OUTPUT	-default"
echo "	- FORWARD"
read -p "Choice[OUTPUT]: " chain

if [ -z "$chain" ]; then
	chain="OUTPUT"
fi

echo "Select protocol:"
echo "	- tcp	-default"
echo "	- udp"
read -p "Choice[tcp]:" protocol

if [ -z "$protocol" ]; then
	protocol="tcp"
fi

echo "Insert port number or range:"
echo "#In this parameter there is not default"
read -p "Port number <1,65535>:" portNumber

numberReg='^[0-9]+$'
if ! [[ $portNumber =~ $numberReg ]]; then
	echo "invalid port number, script aborting"
	exit -1
fi

echo "Select action:"
echo "	- ACCEPT	-default"
echo " 	- DROP"
read -p "Choice[ACCEPT]:" action

if [ -z "$action" ]; then
	action="ACCEPT"
fi


echo "Set comment(optional), leave empty to ignore:"
read -p "Comment text: " comment

if [ ! -z "$comment" ]; then
	comment=" -m comment --comment \"$comment\""
fi

commandString="iptables -I $chain 2 -p $protocol -m $protocol --dport $portNumber -j $action $comment"
echo "Following command will be executed: $commandString"

read -p "Please confirm (y/n) [y]:" confirmed

if [ -z "$confirmed" ]; then
	confirmed="y"
fi

if [ "$confirmed" = "y" ]; then
	echo "Executed"
	eval $commandString
else
	echo "Aborted"
fi

echo
echo
echo
