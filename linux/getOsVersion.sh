#!/bin/bash

__FILE__=`realpath $0`
__DIR__=`dirname $__FILE__`
. "$__DIR__/script_helpers/methods.sh"


lsb_release -a

msg "Kernel version: $(uname -r)"
