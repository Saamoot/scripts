#!/bin/bash   

__FILE__=`realpath $0`
__DIR__=`dirname $__FILE__`
. "$__DIR__/script_helpers/colors.sh"
. "$__DIR__/script_helpers/methods.sh"

msg
msg
msg "${YELLOW}Switch output display port to:${NC}"
msg "\t(1)HDMI port"
msg "\t(2)GPIO port"
msg "\t${RED}(e) exit${NC}"
msg
msg
read -p "Select action: " userChoice
msg
msg

if [ "$userChoice" == "e" ]; then
	msg "${YELLOW}Switch aborted by user.${NC}"
	msg
	msg
	exit -1
fi



confPath="/usr/share/X11/xorg.conf.d/99-fbturbo.conf"
newConfPath="/home/saamoot/scripts/script_configs/"


if [ "$userChoice" == "1" ]; then
	msg "Switching to HDMI port ..."
	#newConfPath="/home/saamoot/scripts/script_configs/99-fbturbo_hdmi.conf"
	newConfPath=$newConfPath"99-fbturbo_hdmi.conf"
	cp "$newConfPath" "$confPath"
fi


if [ "$userChoice" == "2" ]; then
	msg "Switching to GPIO port ..."
	#newConfPath="/home/saamoot/scripts/script_configs/99-fbturbo_spi_lcd.conf"
	newConfPath=$newConfPath"99-fbturbo_spi_lcd.conf"
	cp "$newConfPath" "$confPath"
fi
msg
msg

msg "Configuration file '$newConfPath' copied to '$confPath'"
msg
msg "To apply changes raspberry pi must be restarted."
msg
read -p "Restart(y/n): " restartChoice

if [ "$restartChoice" == "y" -o "$restartChoice" == "Y" ]; then
	shutdown -r now
fi


msg
msg
