#!/bin/bash

__FILE__=`realpath $0`
__DIR__=`dirname $__FILE__`
. "$__DIR__/script_helpers/colors.sh"
. "$__DIR__/script_helpers/methods.sh"


string="Hello world"
length=`strlen "$string"`

echo "Length of '$string' is '$length' char(s)" 