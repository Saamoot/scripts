#!/bin/bash

__FILE__=`realpath $0`
__DIR__=`dirname $__FILE__`
. "$__DIR__/script_helpers/colors.sh"
. "$__DIR__/script_helpers/methods.sh"


msg
msg
msg "${CYAN}This script will change user and group(together named owner) of files to 'www-data:www-data'"
msg "and permission to 775 if not changed. So files and folders belong to apache user.${NC}"
msg

ownerValue="www-data:www-data"

msg "${YELLOW}Setting owner:${NC}"
msg "\tformat of owner is: {username}:{group}"
msg "\tleave empty to use default owner:  $ownerValue"
read -p "Set new owner for files: " newOwner

if [[ $newOwner == "" ]]; then
	newOwner=$ownerValue
fi

msg
msg

permissionsValue="775"
msg "${YELLOW}Setting permission:${NC}"
msg "\tnumber in format xxx, where 'x' is value in range <0,7>"
msg "\tleave empty to use default permissions: $permissionsValue"
msg "\tformat description:"
msg "\t\t- first 'x' is for user/owner of file"
msg "\t\t- second 'x' is for group of file"
msg "\t\t- third 'x' is for other user/groups"
msg "\tvalues descriptions:"
msg "\t\t- 0: nothing allowed"
msg "\t\t- 1: execution allowed"
msg "\t\t- 2: write allowed"
msg "\t\t- 3: write and execution allowed"
msg "\t\t- 4: read allowed"
msg "\t\t- 5: read and execution allowed"
msg "\t\t- 6: read and write allowed"
msg "\t\t- 7: read, write and exeuction allowed"
read -p "Set new permissions for files: " newPermissions

if [[ $newPermissions == "" ]]; then
	newPermissions=$permissionsValue
fi

msg
msg

msg "${YELLOW}Setting path to directory:${NC}"
msg "\tleave empty to use current directory: `pwd`"
read -p "Set directory which should be transfered to apache: " -e directory
if [[ $directory == "" ]]; then
	directory=`pwd`
else
	directory=`realpath $directory`
fi


if [ ! -d $directory ]; then
	msg "${RED}Supplied directory '${YELLOW}$directory${RED}' doesn't exist !"
	exit;
fi

ownerCommand="chown -R $ownerValue $directory"
permissionsCommand="chmod -R $permissionsValue $directory"


msg
msg

msg "${YELLOW}Following commands will be executed:${NC}"
msg "\t$ownerCommand"
msg "\t$permissionsCommand"

msg
msg

read -p "Do you wish to execute them ?[y]: " confirmation

if [[ $confirmation == "" ]]; then
	confirmation="y"
fi


case "${confirmation}" in
	[yY]|[yY][eE][sS])
        	#echo 'Surely this can be written better?' ;;
	;;
	*)
		msg "${RED}Script aborted by user.${NC}"
		msg
		msg
		exit
	;;
esac


msg
msg


msg "${CYAN}Executing commands:${NC}"

msg "\tchanging ownership..."
eval $ownerCommand

msg "\tchanging permissions..."
eval $permissionsCommand

msg
msg "${GREEN}Script finished.${NC}"
msg
msg

