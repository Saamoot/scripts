#!/bin/bash
# script initialisation
__FILE__=`realpath $0`
__DIR__=`dirname $__FILE__`
. "$__DIR__/script_helpers/colors.sh"
. "$__DIR__/script_helpers/methods.sh"
msg
msg "${GREEN}Mount script started ...${NC}"

currentUser=$(whoami)
if [[ $currentUser != "root" ]]; then
	msg "${RED}Script must be run under 'root' user${NC}, aborting script"
	msg
	msg
	exit
fi;

#if [[ `commandExist "cifs"` -le 0 ]]; then
#	msg "${RED}Missing packaged ${YELLOW}'cif-utils'${RED}, aborting script${NC}"
#	msg
#	msg
#	exit
#fi


read -p "Set path to remote directory: " -e remoteDir

if [ ${#remoteDir} -lt 5 ]; then
	msg "${RED} Remote directory path is too short to be real, aborting script${NC}"
	msg
	msg
	exit
fi	

read -p "Path to local directory: " -e localDir
if [ ! -d $localDir ]; then
	msg
	msg "${YELLOW}Supplied directory '$localDir' doesn't exist${NC}"
	read -p "Do you wish to create the folder ?[y/n]: " yn
	case $yn in
		[Yy]*)
			msg "Creating directory '$localDir'..."
			mkdir -p "$localDir"
			if [ ! -d $localDir ]; then
				msg "${RED}Failed do create directory"
				msg "mount-ing won't succeed, aborting script ...${NC}"
				msg
				msg
				exit
			else
				msg "${GREEN}Directory '$localDir' created.${NC}"
				msg
			fi
		;;
		[Nn]*)
			msg "${RED}Destinanation directory wasn't created, mount-ing won't succeed, aborting script${NC}"
			msg
			msg 
			exit
		;;
		*)
			msg
			msg "${YELLOW}Invalid action ${RED}'$yn'${YELLOW} selected, aborting script${NC}"
			msg
			msg
			exit;
		;;
	esac
fi


msg	
msg "Remote folder '$remoteDir' will be mounted to '$localDir'"
msg

msg "Remote directory authentication: "
msg "\t- set username to use when connecting to network drive"
msg "\t- leave username empty to mount without use of username and password"
read -p "Username: " userName

if [[ $userName == "" ]]; then
	msg
	msg "No userName supplied, ommiting username and password"
	userName=""
	password=""		
else
	printf "Please set password for '$userName': "
	read -s password

	password="password=$password,"
	userName="username=$userName,"
fi

msg
msg
msg
defaultSmbVersion="3.0"
msg "SMB Version (vers=):"
msg "\t- if no value is suplied version {$defaultSmbVersion} will be applied"
msg "\t- it is used in case smb server is using older version of smb"
msg "\t\t- which prevents from connecting to it with newer version of smb client"
msg "${YELLOW}example${NC}:"
msg "\t For windows 2003 server, version is ${YELLOW}2.1${NC}, unless updated"
msg
read -p "Smb version: " smbVersion

if [[ $smbVersion != "" ]]; then
	smbVersion=",vers=$smbVersion"
else
	smbVersion=",vers=${defaultSmbVersion}"
fi



command="mount -t cifs $remoteDir $localDir -o ${userName}${password}iocharset=utf8,file_mode=0777,dir_mode=0777${smbVersion}"

msg
msg
msg "${CYAN}Attempting to mount with command:${NC} \n\t ${YELLOW}$command${NC}"
eval $command
msg
msg
msg "${PURPLE}Mount script finished, bye.${NC}"
msg
msg

