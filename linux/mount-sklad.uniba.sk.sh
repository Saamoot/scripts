#!/bin/bash

echo
echo
echo "Mount script started ..."

currentUser=$(whoami)

if [[ $currentUser != "root" ]]; then
	echo "Script must be run under 'root' user"
	echo
	echo
	exit;
fi;



remoteDir="//sklad.uniba.sk/data"
localDir="/media/matus11/sklad.uniba.sk"

echo "Mounting '$remoteDir' to '$localDir' ..."
echo


printf "Please set username to use to connect to network drive: "
read userName

if [[ $userName == "" ]]; then
	echo "No userName supplied, default 'matus11' will be used"
	userName='matus11'
fi

echo
printf "Please set password for '$userName': "
read -s password

mount -t cifs $remoteDir $localDir -o username=$userName,password=$password,iocharset=utf8,file_mode=0777,dir_mode=0777,vers=2.1
echo
echo
echo "Mount script finished, bye."
echo
echo
