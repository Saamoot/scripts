#!/bin/bash

echo
echo

echo "Stop all openvpn connections script, started ..."

currentUser=$(whoami)

if [[ "$currentUser" != "root" ]]; then
	echo "Script must be run under 'root' user, script stopped, bye.'"
	echo
	echo
	exit
fi


killall openvpn &

echo "Script to stop all openvpn connections finished, bye"
echo
echo

