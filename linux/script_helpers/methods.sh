#!/bin/bash 

function msg {
	printf "$1\n"
}

function strlen {
	echo "${#1}"
}

function commandExist {
	result=`command -v $1`
	result=`strlen $result`
	if [[ $result -gt 0 ]]; then
		echo "1"
	else
		echo "0"
	fi
}

