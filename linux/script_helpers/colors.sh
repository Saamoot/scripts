#!/bin/bash  

#CK="\033[30m"
#RED="\033[31m"
#GREEN="\033[32m"
#YELLOW="\033[33m"
#BLUE="\033[34m"
#PINK="\033[35m"
#CYAN="\033[36m"
#WHITE="\033[37m"
#NC="\033[0;39m"

NC='\e[0m' # No Color
WHITE='\e[1;37m'
BLACK='\e[0;30m'
BLUE='\e[0;34m'
LIGHT_BLUE='\e[1;34m'
GREEN='\e[0;32m'
LIGHT_GREEN='\e[1;32m'
CYAN='\e[0;36m'
LIGHT_CYAN='\e[1;36m'
RED='\e[0;31m'
LIGHT_RED='\e[1;31m'
PURPLE='\e[0;35m'
LIGHT_PURPLE='\e[1;35m'
BROWN='\e[0;33m'
YELLOW='\e[1;33m'
GRAY='\e[0;30m'
LIGHT_GRAY='\e[0;37m'
