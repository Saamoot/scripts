#!/bin/bash


echo
echo
echo "OpenVpn-connect started..."
echo

printf "Set path to '*.openvpn' file: "
read -e path

if [ -z "$path" ]; then
	#echo "Path cannot be empty, stopping script, bye."
	path="/home/matus11/openvpn-config/uniba/uniba.ovpn"
	echo "Setting default config file '$path'"
	echo
	echo
	#exit
fi

if [ ! -f "$path" ]; then
	echo "File doesn't exist, stopping script, bye."
	echo
	echo
	exit
fi

currentDir="$(pwd)"
configFileDir=$(dirname $path)

echo "Switching directory"
echo -e "\tfrom: $currentDir"
echo -e "\tto: $configFileDir"
cd $configFileDir

echo
openvpn --config $path &
echo
echo "Restoring initial directory"
cd $currentDir
echo
echo "OpenVpn-connect triggered, script is done, bye."
echo
echo

