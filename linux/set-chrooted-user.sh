#!/bin/bash
# script initialisation
__FILE__=`realpath $0`
__DIR__=`dirname $__FILE__`
. "$__DIR__/script_helpers/colors.sh"
. "$__DIR__/script_helpers/methods.sh"

msg
msg "${LIGHT_BLUE}Creation of chroot-ed user script started ...${NC}"

currentUser=$(whoami)
if [[ $currentUser != "root" ]]; then
	msg "${RED}Script must be run under 'root' user${NC}, aborting script"
	msg
	msg
	exit
fi;

msg
msg "${YELLOW}Script pre-configuration${NC}"
msg
msg

msg "${YELLOW}Chroot-ing by group${NC}"
msg "\t- leave empty for default ${YELLOW}sftponly${NC}"
msg "\t- this group is used to chroot local user"
msg "\t- this group is used on chroot-ed directories"
read -p "Group name: " group

if [[ "${group}" == "" ]]; then
	group="sftponly"
fi

msg
msg "Checking if \"$group\" group exist"
msg
groupExist=`getent group "$group"`

if [[ "$groupExist" == "" ]]; then
	msg "${RED}Script cannot continue, because group \"$group\" doesn't exist${NC}"
	msg
	msg
	exit
fi

msg
msg "${YELLOW}Web server application${NC}"
msg "\t- leave empty for ${YELLOW}www-data${NC} apache user"
msg "\t- or set if custom user or another web server application user"
read -p "Web user: " webUser

if [[ "${webUser}" == "" ]]; then
	webUser="www-data"
fi

msg
msg "Checking if web server application user and group \"${webUser}\" exists"
msg

webUserExist=`getent passwd "$webUser"`

if [[ "${webUserExist}" == "" ]]; then
	msg
	msg "${RED}Script cannot continue, specified web user \"${webUser}\" doesn't exist${NC}"
	msg
	msg
	exit
fi

webUserGroupExist=`getent group "$webUser"`

if [[ "${webUserGroupExist}" == "" ]]; then
	msg
	msg "${RED}Script cannot continue, specified web user \"${webUser}\"  doesn't have group \"${webUser}\"${NC}"
	msg
	msg
	exit
fi


msg
msg "${YELLOW}User creation options${NC}"
msg "\t- new local user will be created"
msg "\t- user home directory will be created"
msg "\t- ssh access to terminal for user will be disabled"
msg "\t- sftp authentication will be done via private/public key"

read -p "Set user name: " userName

if [[ "$userName" == "" ]]; then
	msg
	msg
	msg "${RED}User name cannot be empty${NC}"
	msg
	msg
	exit
fi


msg
msg "Checking if user \"$userName\" exist"

userExist=`getent passwd "$userName"`

if [[ "$userExist" != "" ]]; then
	msg
	msg
	msg "${RED}User with name \"${userName}\" already exist${NC}"
	msg
	msg
	exit
fi

################
#              #
# USER OPTIONS #
#              #
################
isConfig=""

msg
msg "${YELLOW}User web dir${NC}"
msg "\t- set to ${YELLOW}y${NC} or ${YELLOW}Y${NC} to create web dir and symlink to it"
msg "\t- use this option if user plan to have website running on server"
msg "\t- use this option if user plan to store files with public access"
read -p "Create user web structure: " isWeb

if [[ "${isWeb}" != "y" && "${isWeb}" != "Y" ]]; then
	msg
	msg "${BLUE}Web structure will be omitted${NC}"
	msg
else
	isConfig="y"
	isWeb="y"
fi

msg
msg "${YELLOW}User data dir${NC}"
msg "\t- set to ${YELLOW}y${NC} or ${YELLOW}Y${NC} to create data dir"
msg "\t- directory where use can store files without public access"
read -p "Create user data structure: " isData

if [[ "${isData}" != "y" && "${isData}" != "Y" ]]; then
	msg
	msg "${BLUE}Data structure will be omitted${NC}"
	msg
else
	isData="y"
fi
msg
msg




# PATHS
pathSystemWebFolder="/var/www"
pathSystemUsersDir="/home"
pathUserWebFolder="${pathSystemWebFolder}/${userName}"
pathUserHomeDir="${pathSystemUsersDir}/${userName}"
pathUserSymlinkWeb="${pathUserHomeDir}/www"
pathUserDataDir="${pathUserHomeDir}/data"
pathUserConfigDir="${pathUserHomeDir}/config"

# SYSTEM COMMANDS
commandCreateUser="useradd -m ${userName} -g ${group} -s /bin/false"
commandCreateWebSymlink="ln -s ${pathSystemWebFolder} ${pathUserSymlinkWeb}"
commandChownHomesDir="chown root:root ${pathSystemUsersDir}"
commandChownUserHomeDir="chown root:root ${pathUserHomeDir}"
commandChownUserFiles="chown ${userName}:${group} ${pathUserHomeDir}/*"
commandChmodHomesDir="chmod 755 ${pathSystemUsersDir}"
commandChmodUserHomeDir="chmod 755 ${pathUserHomeDir}"
commandChmodUserFiles="chmod -R 775 ${pathUserHomeDir}/*"
commandCreateDataDir="mkdir ${pathUserHomeDir}/data"
commandCreateConfDir="mkdir ${pathUserHomeDir}/conf"
commandAddApacheUserToUserGroup="usermod -g ${userName} ${webUser}"

# USER COMMANDS
commandCreateUserSystemWebfolder="mkdir ${pathUserWebFolder}"
commandChownUserSystemWebFolder="chown ${userName}:${userName} ${pathUserWebFolder}"
commandChmodUserSystemWebFolder="chmod 770 ${pathUserWebFolder}"



msg "${YELLOW}Checking system web dir path \"${pathSystemWebFolder}\"${NC}"
if [[ ! -d "${pathSystemWebFolder}" ]]; then
	msg "${RED}Script cannot continue, system web dir deosn't exist${NC}"
	msg
	msg
	exit
else
	msg "[${GREEN}ok${NC}]"
	msg
fi

msg
msg "${YELLOW}User will be created with command: ${NC}"
msg "\t${commandCreateUser}"
msg

read -p "Continue script? [y|Y]:" -r
msg

if [[ ! "${REPLY}" =~ ^[Yy]$ ]]; then
	msg
	msg
	msg "${RED}Script aborted by user${NC}"
	msg
	msg
	exit
fi

msg
msg "${YELLOW}Check directories and commands for their creation${NC}"
if [[ "${isData}" == "y" ]]; then
	msg "\t- user data dir \"${YELLOW}${pathUserDataDir}${NC}\""
	msg "\t\t- create command: \"${YELLOW}${commandCreateConfDir}${NC}\""
fi

if [[ "${isWeb}" == "y" ]]; then
	msg "\t- user system web dir \"${YELLOW}${pathUserWebFolder}${NC}\""
	msg "\t\t- create command: \"${YELLOW}${commandCreateUserSystemWebfolder}${NC}\""
	msg "\t\t-  chown command: \"${YELLOW}${commandChownUserSystemWebFolder}${NC}\""
	msg "\t\t-  chmod command: \"${YELLOW}${commandChmodUserSystemWebFolder}${NC}\""
	msg "\t- user symlink to system web dir \"${YELLOW}${pathUserSymlinkWeb}${NC}\""
	msg "\t\t- create command: \"${YELLOW}${commandCreateWebSymlink}${NC}\""
fi

if [[ "${isConfig}" == "y" ]]; then
	msg "\t- user configuration dir \"${YELLOW}${pathUserConfigDir}${NC}\""
	msg "\t\t- create command: \"${YELLOW}${commandCreateConfDir}${NC}\""
fi

msg
read -p "Continue script? [y|Y]:" -r
msg
if [[ ! "${REPLY}" =~ ^[Yy]$ ]]; then
	msg
	msg
	msg "${RED}Script aborted by user${NC}"
	msg
	msg
	exit
fi
